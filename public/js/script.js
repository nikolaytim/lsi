jQuery(document).ready(function() {
    $.datepicker.regional['en'] = {
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['en']);

    $("#datepickerFrom").datepicker();
    $('#datepickerFrom').datepicker('setDate', (new Date()) );
    $("#datepickerTo").datepicker();
    $('#datepickerTo').datepicker('setDate', new Date());

    $('#submit').on('click', function () {
        let from = $('#datepickerFrom').datepicker('getDate').toLocaleDateString().split('.').reverse().join('-')
        let to = $('#datepickerTo').datepicker('getDate').toLocaleDateString().split('.').reverse().join('-')
        let location = $('#location').val();

        $.ajax({
            async: false,
            url: '/Api/GetExportsByFilter/v1?from=' + from + '&to=' + to + '&location=' + location,
            type: 'GET',
            processData: false,
            contentType: false,
        }).then(function (result) {
            $('#result').html(result);
        });

    });
});