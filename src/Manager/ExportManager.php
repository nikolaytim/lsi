<?php

namespace App\Manager;

use App\Entity\Export;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class ExportManager
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function add(string $name, DateTime $createdAt, string $userName, string $location): void
    {
        $export = new Export();
        $export->setName($name);
        $export->setCreatedAt($createdAt);
        $export->setUserName($userName);
        $export->setLocation($location);
        $this->entityManager->persist($export);
        $this->entityManager->flush();
    }
}