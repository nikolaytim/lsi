<?php

namespace App\Command;

use App\Service\TestDataService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestDataCommand extends Command
{
    private TestDataService $testDataService;

    public function __construct(TestDataService $testDataService)
    {
        parent::__construct();
        $this->testDataService = $testDataService;
    }

    protected function configure(): void
    {
        $this->setName('testdata:add')
            ->setDescription('Add test data');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->write("<info>Begin: " . (new \DateTime())->format('d.m.Y H:i:s') . "</info>\n");
        $this->testDataService->addTestData();
        $output->write("<info>Finish: " . (new \DateTime())->format('d.m.Y H:i:s') . "</info>\n");
    }
}