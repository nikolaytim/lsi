<?php

namespace App\Handler;

use App\Entity\Export;
use Doctrine\ORM\EntityManagerInterface;

class ExportsByFilterHandler
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handler(string $from, string $to, string $location): array
    {
        $exportRepository = $this->entityManager->getRepository(Export::class);
        return $exportRepository->findExportsByFilter($from, $to, $location);
    }
}