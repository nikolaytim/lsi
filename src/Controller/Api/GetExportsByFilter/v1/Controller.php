<?php

namespace App\Controller\Api\GetExportsByFilter\v1;

use App\Handler\ExportsByFilterHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\Routing\Annotation\Route as Route;

class Controller extends AbstractController
{
    private Environment $environment;
    private ExportsByFilterHandler $handler;

    public function __construct(Environment $environment, ExportsByFilterHandler $handler)
    {
        $this->environment = $environment;
        $this->handler = $handler;
    }

    /**
     * @Route("/Api/GetExportsByFilter/v1")
    */
    public function getExportsByFilterAction(Request $request): Response
    {
        return new Response(
            $this->environment->render(
                'exports_by_filter.html.twig',
                ['exports' => $this->handler->handler(
                    $request->query->get('from'),
                    $request->query->get('to'),
                    $request->query->get('location')
                )])
        );
    }
}