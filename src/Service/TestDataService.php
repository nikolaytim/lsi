<?php

namespace App\Service;

use App\Manager\ExportManager;
use DateTime;

class TestDataService
{
    private ExportManager $exportManager;

    public function __construct(ExportManager $exportManager)
    {
        $this->exportManager = $exportManager;
    }

    public function addTestData(): void
    {
        $testData = [
            [
                'name' => 'Export1',
                'createdAt' => new DateTime('2022-01-01 10:00:00'),
                'userName' => 'Ivan',
                'location' => 'Europe'
            ],
            [
                'name' => 'Export2',
                'createdAt' => new DateTime('2022-01-01 11:00:00'),
                'userName' => 'Petr',
                'location' => 'Europe'
            ],
            [
                'name' => 'Export3',
                'createdAt' => new DateTime('2022-01-01 15:00:00'),
                'userName' => 'Ivan',
                'location' => 'Africa'
            ],
            [
                'name' => 'Export4',
                'createdAt' => new DateTime('2022-01-01 12:00:00'),
                'userName' => 'Masha',
                'location' => 'Africa'
            ],
            [
                'name' => 'Export5',
                'createdAt' => new DateTime('2022-01-01 12:00:00'),
                'userName' => 'Ivan',
                'location' => 'Europe'
            ]
        ];

        foreach ($testData as $item) {
            $this->exportManager->add($item['name'], $item['createdAt'], $item['userName'], $item['location']);
        }
    }
}