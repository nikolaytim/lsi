<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ExportRepository extends EntityRepository
{
    public function findExportsByFilter(string $from, string $to, string $location): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('e.name', 'e.createdAt', 'e.userName', 'e.location')
            ->from('App\Entity\Export', 'e')
            ->where('e.location = :location')
            ->andWhere('e.createdAt BETWEEN :fromDate AND :toDate')
            ->setParameter('location', $location)
            ->setParameter('fromDate', $from)
            ->setParameter('toDate', $to);

        return $qb->getQuery()->getResult();
    }
}